module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_name    = "my-eks"
  #If you have a bastion host or a VPN, you can enable a private endpoint
  cluster_endpoint_private_access = true

  # to access to vpc from my laptop
  cluster_endpoint_public_access  = true

  vpc_id          = module.vpc.vpc_id

  #A list of subnets to place the EKS cluster and workers within.
  subnets         = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]


  enable_irsa = true

  node_groups_defaults = {
    disk_size = 50
  }

  node_groups = {
    general = {
      desired_size = 1
      min_size     = 1
      max_size     = 10

      labels = {
        role = "general"
      }

      instance_types = ["t2.micro"]
      capacity_type  = "ON_DEMAND"
    }

    spot = {
      desired_size = 1
      min_size     = 1
      max_size     = 10

      labels = {
        role = "spot"
      }

      taints = [{
        key    = "market"
        value  = "spot"
        effect = "NO_SCHEDULE"
      }]

      instance_types = ["t2.micro"]
      capacity_type  = "SPOT"
    }
  }

  tags = {
    Environment = "staging"
  }
}
